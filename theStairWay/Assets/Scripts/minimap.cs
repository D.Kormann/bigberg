﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class minimap : MonoBehaviour
{
    private static minimap map;
    private void Awake()
    {
        map = this;
        
    }
    public static void updatePos(float x, float y)
    {
        x -= .5f;
        y -= .5f;
        map.transform.localPosition = new Vector3(x, y)*100f;
    }
}
