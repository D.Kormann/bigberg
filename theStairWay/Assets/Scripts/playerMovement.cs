﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class playerMovement : MonoBehaviour
{
    public Tiles tiles;
    player P;
    float speed;
    Vector3 target;
    List<Vector3> nextSteps=new List<Vector3>();
    float lastStep;
    float stepTime;
    Vector3 nextPos;

    bool isStepping;
    private void Start()
    {
        
        P = Figure.P;
        
        float h = tiles.randomHeight((int)P.position.x, (int)P.position.y);
        int cleanx = (int)(P.position.x / Tiles.size) * Tiles.size;
        int cleany = (int)(P.position.y / Tiles.size) * Tiles.size;
        P.position = new Vector3(cleanx, cleany, h);
        transform.position = P.position;
        speed = P.speed;
        stepTime = 1 / P.speed;
        nextPos = P.position;

    }

    private void Update()
    {
        updatePos();
    }

    

    private void updatePos()//wird jede frame ausgeführt
    {
        if (isStepping)//sind wir grade dabei einen schritt zu machen?
        {
            float t = (Time.time - lastStep) / stepTime;//verbleibende zeit für den schritt
            if (t >= 1)//wenn der schritt fertig ist 
            {
                t -= 1;
                P.position = nextPos;
                tiles.updatePos(nextPos);
                if (nextSteps.Count == 0)//sind weitere schritte geplant?
                {
                    isStepping = false;
                }
                else
                {
                    nextPos = nextSteps[0];
                    nextSteps.RemoveAt(0);
                    lastStep += stepTime;
                }
            }
            transform.position = Vector3.Lerp(P.position, nextPos, t);
        }

    }
    public void setTarget(Vector3 target)
    {
        int x = (int)(target.x);
        int y = (int)(target.y);
        
        if (x < -Tiles.worldDepth)
            x = -Tiles.worldDepth;
        else if (x > Tiles.worldDepth)
            x = Tiles.worldDepth;

        if (y < 0)
            y = 0;
        else if (y > Tiles.worldDepth)
            y = Tiles.worldDepth;

        if (target == nextPos)
        {
            return;
        }
        nextSteps.Clear();
        Vector3 futurePos=nextPos;
        Vector3 prepos;
        while (x > nextPos.x)
        {
            x--;
            prepos = futurePos;
            futurePos = futurePos + Vector3.right;
            futurePos=stepUp(futurePos, prepos);

            
            
        }
        while (x < nextPos.x)
        {
            x++;
            prepos = futurePos;
            futurePos = futurePos + Vector3.left;
            futurePos = stepUp(futurePos, prepos);

            
        }
        while (y > nextPos.y)
        {
            y--;
            prepos = futurePos;
            futurePos = futurePos + Vector3.up;
            futurePos = stepUp(futurePos, prepos);

            
        }
        while (y < nextPos.y)
        {
            y++;
            prepos = futurePos;
            futurePos = futurePos + Vector3.down;
            futurePos = stepUp(futurePos, prepos);

            
        }

        if (!isStepping)
        {
            nextPos = nextSteps[0];
            nextSteps.RemoveAt(0);
            isStepping = true;
            lastStep = Time.time;
        }
    }

    Vector3 stepUp(Vector3 futurePos,Vector3 prepos)
    {
        int h = Tiles.getHeightAt((int)futurePos.x, (int)futurePos.y);

        if (h == futurePos.z)//walking on plain field
        {
            nextSteps.Add(futurePos);

        }
        else
        {
            if (h > futurePos.z)//stepping down
            {
                if (h <= 0)//dont step under whater;
                {

                    nextSteps.Add(futurePos);
                    futurePos += Vector3.forward;
                    nextSteps.Add(futurePos);
                }

            }
            else//stepping up
            {
                prepos += Vector3.back;
                nextSteps.Add(prepos);
                if (h + 1 == futurePos.z)//if we managed to step up u move on the plattform
                {
                    futurePos += Vector3.back;
                    nextSteps.Add(futurePos);
                }
                else
                {
                    futurePos = prepos;//we cant get to planned future position
                }

            }
            h = Tiles.getHeightAt((int)futurePos.x, (int)futurePos.y);//recalculate the height of the ground
            float fallingspeed=0;
            while (h>futurePos.z)//check if we are gonna fall down
            {
                fallingspeed++;//increasing fallingspeed by 1 until we ht ground
                futurePos+=Vector3.forward* Mathf.Min(fallingspeed , h - futurePos.z);//moving to ground
                nextSteps.Add(futurePos);
            }



        }
        

        return futurePos;//return the position we gonna be at
    }
    

    public void Up()
    {
        setTarget(nextPos + Vector3.up);
    }
    public void Down()
    {
        setTarget(nextPos + Vector3.down);
    }
    public void Right()
    {
        setTarget(nextPos + Vector3.right);
    }
    public void Left()
    {
        setTarget(nextPos + Vector3.left);
    }
   
}
