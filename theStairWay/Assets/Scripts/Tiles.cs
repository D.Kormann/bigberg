﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class Tiles : MonoBehaviour

{
    public Texture2D t2d;

    public Color whater,c1, c2, topColor;
    public int _worldDepth;
    public static int worldDepth;
    public static int size = 21;
    static int halfsize=size/2;
    public Transform[] tiles;
    public Vector3 pos;
    public Transform[][] Rows = new Transform[size][];
    private SpriteRenderer[][] renderers = new SpriteRenderer[size][];
    static int[][] heights = new int[size][];
    

    public Material material;
    private void Awake()
    {
        worldDepth = t2d.height;

    }
    private void Start()
    {
        pos = new Vector3(Figure.P.position.x, Figure.P.position.y, 0);
        setupTiles();
        updatePos(new Vector3(0,0,0));
        updatePos(new Vector3(0,0,0));
        updatePos(new Vector3(0,0,0));

        
    }
    private void setTileAt(Vector3 dir, int x ,int y)
    {
        Vector3 temp = Rows[x][y].position;
        temp += dir * size;
        int h = randomHeight((int)temp.x, (int)temp.y);
        heights[x][y] = h;
        Rows[x][y].position = new Vector3(temp.x, temp.y, h);
        renderers[x][y].color = getColorAt(x, y);
    }
    public void Move(Vector3 dir)
    {
        
        if (dir == Vector3.right)
        {
            int relX = mod((int)pos.x, size);
            for (int i = 0; i < size; i++)
            {
                setTileAt(dir, relX, i);
                
            }
        }
        else if (dir == Vector3.left)
        {
            int relX = mod((int)pos.x-1, size);
            for (int i = 0; i < size; i++)
            {
                setTileAt(dir, relX, i);
            }
        }
        else if(dir == Vector3.up)
        {
            
            int relX = mod((int)pos.y, size);
            for (int i = 0; i < size; i++)
            {
                setTileAt(dir,i, relX);

            }
        }
        else if(dir == Vector3.down)
        {
            
            int relX = mod((int)pos.y-1, size);
           
            for (int i = 0; i < size; i++)
            {
                setTileAt(dir, i, relX);
            }
        }
        pos += dir;
    }
    void setupTiles()
    {
        
        for (int i = 0; i < size; i++)
        {
            Rows[i] = new Transform[size];
            renderers[i] = new SpriteRenderer[size];
            heights[i] = new int[size];
        }
        

        for (int i = 0; i < size*size; i++)
        {
            Transform current = i < tiles.Length ? tiles[i] : Instantiate(tiles[0]);
            int x = i % size;
            int y = i / size;
            int glox = x + (int)pos.x;
            int gloy = y + (int)pos.y;
            int h = randomHeight(glox, gloy);
            current.position = new Vector3(glox, gloy, h);
            Rows[x][y] = current;
            renderers[x][y] = current.GetComponent<SpriteRenderer>();
            heights[x][y] = h;
            renderers[x][y].color = getColorAt(glox, gloy);
            
        }



    }

    static int mod(int x,int y)
    {
        if (x < 0)
        {
            x -= x*y;
        }
        return x % y;
    }
    public void updatePos(Vector3 newPos)
    {
        int x = (int)(newPos.x + worldDepth+.5f) - worldDepth-halfsize;
        int y = (int)(newPos.y + worldDepth+.5f) - worldDepth-halfsize+1;
        

        if (x > pos.x+.2f)
        {
            Move(Vector3.right);
        }
        else if (x<pos.x-.2f)
        {
            Move(Vector3.left);
        }
        
        
        if (y > pos.y+.2f)
        {
            Move(Vector3.up);
        }
        else if(y<pos.y-.2f)
        {
            Move(Vector3.down);
        }

        minimap.updatePos(newPos.x / worldDepth, newPos.y / worldDepth);
    }
    private Color getColorAt(int x, int y)
    {
        int h = getHeightAt(x, y);
        if (h > 0)//under whater
        {
            return whater;
        }
        float t = x * 6.5f + 3.3f + y * 7.2f;
        t %= 1f;
        Color C = Color.Lerp(c1, c2, t);
        C = Color.Lerp(C, topColor, -(float)h / 256f);
        
        return C;

    }
    public static int getHeightAt(int x,int y)
    {
        return heights[mod(x, size)][mod(y, size)];
    }
    public int randomHeight(int x,int y)
    {
        int h= 1-(int)(t2d.GetPixel(x, y).r * 256);
        

        return h;

        
    }
}
