﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class whater : MonoBehaviour
{

    Transform cam;
    Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.allCameras[0].transform;
        offset = -cam.position + transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(cam.position.x + offset.x, cam.position.y + offset.y, 0f);
    }
}
