﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tile : MonoBehaviour
{
    
    SpriteRenderer spr;
    bool mouseOver;
    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
    }
    
    private void OnMouseEnter()
    {
        spr.color = Color.yellow;
    }

    private void OnMouseExit()
    {
        spr.color = Color.clear;
    }
    private void OnMouseUpAsButton()
    {
        Figure.P.mover.setTarget(transform.position);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireCube(transform.position-Vector3.back, new Vector3(1,1,2));
    }
}
