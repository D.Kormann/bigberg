﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Wheel : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public RectTransform background;
    public Transform viewAngle;
    Canvas canvas;
    Camera cam;
    Vector2 input;
    Vector2 position;
    float angle,startAngle;
    bool dragged;
    playerMovement mover;

    public void Start()
    {
        canvas = GetComponentInParent<Canvas>();
        cam = canvas.worldCamera;
        position = RectTransformUtility.WorldToScreenPoint(cam, background.position);
        mover = Figure.P.GetComponent<playerMovement>();
        background.transform.rotation = Quaternion.identity;


    }
    public void OnDrag(PointerEventData eventData)
    {

        angle = getInputAngle(eventData)-startAngle;
        background.transform.rotation = Quaternion.identity;
        background.transform.Rotate(0, 0, angle);
        viewAngle.rotation = Quaternion.identity;
        viewAngle.Rotate(0, 0, -angle);
        dragged = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        startAngle = getInputAngle(eventData)-angle;
        dragged = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        //keep the angle in the right size
        startAngle = startAngle < -180 ? startAngle + 360:startAngle;
        startAngle = startAngle > 180 ? startAngle - 360 : startAngle;
        if (!dragged)
        {
            if (startAngle < -135f)//down
            {
                print("down");
                mover.Down();
            }
            else if(startAngle<-45f)//right
            {
                print("right");
                mover.Right();
            }
            else if (startAngle < 45f)//up
            {
                print("up");
                mover.Up();
            }
            else if(startAngle<135)//left
            {
                print("left");
                mover.Left();
            }
            else//down again
            {
                print("down");
                mover.Down();
            }
        }
    }


    private float getInputAngle(PointerEventData eventData)
    {
        Vector2 input = handleInput(eventData);
        return Vector2.SignedAngle(Vector2.up, input);
    }
    private Vector2 handleInput(PointerEventData eventData)
    {
        return eventData.position - position;
    }
}
